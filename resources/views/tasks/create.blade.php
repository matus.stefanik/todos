@extends("layouts.app")


@section("content")

<h1>Create task</h1>
<form method="POST" action="/tasks">
    <div class="form-group">
        @csrf
        <label for="description">task description</label>
        <input type="text" class="form-control" name="decription" >

    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">create task</button>
    </div>
</form>

@endsection